# bootstrap

bootstrap a k8s cluster with [cert-manager][], [istio][], [oauth2-proxy][] and [openebs-dynamic-nfs-provisioner][]

[cert-manager]: https://cert-manager.io/
[istio]: https://istio.io/
[oauth2-proxy]: https://oauth2-proxy.github.io/oauth2-proxy/
[openebs-dynamic-nfs-provisioner]: https://openebs.io/docs/
