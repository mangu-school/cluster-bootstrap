## apps installed
After boostrapping our cluster, we install some sample apps to verify that the 
things are where and how they should be.

List of apps installed, by order of installation:

| order |       app name           | version |  namespace   |  Status  |
|:-----:|:------------------------:|:-------:|:------------:|:--------:|
|   1   |  [productpage][bookinfo] | 1.16.4  | productpage  | Deployed |

**NOTE:**

productpage is a single microservice from [bookinfo][bookinfo]

[bookinfo]: https://github.com/istio/istio/tree/master/samples/bookinfo
