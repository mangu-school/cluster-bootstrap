# cluster bootstrap

bootstrap a k8s cluster with [cert-manager][], [istio][], [oauth2-proxy][] and [openebs-dynamic-nfs-provisioner][]

## system requirements
To drive things in this repo, the following packages are required:

1. [GNU Make][]
2. [kubectl][]
3. [kustomize][]
5. [helm][]

## stacks installed
List of stacks installed, by order of installation:

| order |       app name       | version |  namespace   |  Status  |
|:-----:|:--------------------:|:-------:|:------------:|:--------:|
|   0   |  [k8tz][]            |   N/A   |     k8tz     | Deployed |
|   1   |  [cert-manager][]    | v1.8.0  | cert-manager | Deployed |
|   2   |  [istio][]           | v1.16.1 | istio-system | Deployed |
|   3   |  [oauth2-proxy][]    | v7.3.0  |   default    | Deployed |


[GNU Make]: https://www.gnu.org/software/make/
[kubectl]: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
[kustomize]: https://kubectl.docs.kubernetes.io/installation/kustomize/
[helm]: https://github.com/helm/helm/releases

[k8tz]: https://github.com/k8tz/k8tz
[cert-manager]: https://cert-manager.io/
[istio]: https://istio.io/
[oauth2-proxy]: https://oauth2-proxy.github.io/oauth2-proxy/
[openebs-dynamic-nfs-provisioner]: https://openebs.io/docs/
