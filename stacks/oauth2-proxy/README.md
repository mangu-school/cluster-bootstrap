# oauth2-proxy

See example [here][]

```shell
helm repo add oauth2-proxy https://oauth2-proxy.github.io/manifests

helm repo update

helm install \
  --namespace auth \
  --values oauth2-proxy-values.yaml \
  --version 5.0.6 \
  oauth2-proxy oauth2-proxy/oauth2-proxy
```

[here]: https://github.com/vanneback/istio-external-auth-demo#installing-oauth2-proxy
