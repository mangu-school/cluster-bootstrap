## istio kustomization

Refer to istio-csr examples for [istio configured with istio-csr][].

[See this for another example][] that shows installing istio with pa rivate CA.

#### get istioctl
```shell
curl -L https://istio.io/downloadIstio | ISTIO_VERSION=1.16.1 TARGET_ARCH=x86_64 sh -
```

### generate manifests
```shell
tmp/istio-1.16.1/bin/istioctl manifest generate -f install-profile.yaml -o base

# or simply
make base-istio
```

**NOTE:** 

Need to replace all instances of `policy/v1` with `policy/v1`

Need to replace all instances of `autoscaling/v2beta2` with `autoscaling/v2`


[istio configured with istio-csr]: https://github.com/cert-manager/istio-csr/blob/main/docs/istio-config-getting-started.yaml
[See this for another example]: https://smallstep.com/blog/istio-with-private-ca/
