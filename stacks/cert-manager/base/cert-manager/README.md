# namespaces created

|     name     |        annotations        |
|:------------:|:-------------------------:|
| cert-manager |            N/A            |
| istio-system | istio-injection: disabled |
|     dev      | istio-injection: enabled  |
